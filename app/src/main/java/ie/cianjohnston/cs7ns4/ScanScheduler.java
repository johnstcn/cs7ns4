package ie.cianjohnston.cs7ns4;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import com.google.gson.Gson;
import ie.cianjohnston.cs7ns4.SubmitTask.SubmitTaskResult;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

class ScanScheduler {
  private static final String TAG = "cs7ns4.ScanScheduler";

  private Handler handler;
  private boolean active;
  private Scanner scanner;
  private Runnable action;
  private Gson gson;
  private List<ScanResult> buffer;
  private String submitUrl;
  private String apiKey;
  private int intervalMs;

  ScanScheduler(
      final Scanner scanner, final String submitUrl, final String apiKey, final int intervalMs) {
    this.handler = new Handler();
    this.gson = new Gson();
    this.active = false;
    this.scanner = scanner;
    this.buffer = new ArrayList<>();
    this.submitUrl = submitUrl;
    this.apiKey = apiKey;
    this.intervalMs = intervalMs;
    this.action =
        () -> {
          buffer.addAll(ScanScheduler.this.scanner.scan());
          final String payload = gson.toJson(buffer);
          final SubmitTaskResult res;
          try {
            res =
                new SubmitTask()
                    .execute(
                        ScanScheduler.this.getSubmitUrl(), ScanScheduler.this.getApiKey(), payload)
                    .get();
          } catch (ExecutionException e) {
            e.printStackTrace();
            return;
          } catch (InterruptedException e) {
            e.printStackTrace();
            return;
          }
          if (null != res.getError()) {
            Log.e(
                TAG,
                "submitting "
                    + buffer.size()
                    + " scan results to "
                    + ScanScheduler.this.getSubmitUrl()
                    + ": "
                    + res.getError().getMessage());
          } else {
            Log.i(
                TAG,
                "submitted "
                    + buffer.size()
                    + " scan results to "
                    + ScanScheduler.this.getSubmitUrl());
            buffer.clear();
          }
          ScanScheduler.this.handler.postDelayed(
              ScanScheduler.this.action, ScanScheduler.this.getIntervalMs());
        };
  }

  String getSubmitUrl() {
    return submitUrl;
  }

  String getApiKey() {
    return apiKey;
  }

  int getIntervalMs() {
    return intervalMs;
  }

  void setSubmitUrl(final String newSubmitUrl) {
    submitUrl = newSubmitUrl;
  }

  void setApiKey(final String newApiKey) {
    apiKey = newApiKey;
  }

  void setIntervalMs(final int newIntervalMs) {
    intervalMs = newIntervalMs;
  }

  void start() {
    if (active) {
      return;
    }

    active = true;
    handler.post(action);
    Log.i(TAG, "started scheduling scans");
  }

  void stop() {
    if (!active) {
      return;
    }

    handler.removeCallbacks(action);
    active = false;
    Log.i(TAG, "stopped scheduling scans");
  }

  void toggle() {
    if (active) {
      stop();
    } else {
      start();
    }
  }
}
