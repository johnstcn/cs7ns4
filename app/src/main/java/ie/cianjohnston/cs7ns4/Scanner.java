package ie.cianjohnston.cs7ns4;

import android.location.Location;
import android.location.LocationManager;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

class Scanner {
  private static final String TAG = "cs7ns4.Scanner";
  private final TelephonyManager tm;
  private final LocationManager lm;

  Scanner(final TelephonyManager tm, final LocationManager lm) {
    this.tm = tm;
    this.lm = lm;
  }

  List<ScanResult> scan() {
    final List<ScanResult> results = new ArrayList<>();
    final Location loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

    if (null == loc) {
      Log.e(TAG, "no location available");
      return results;
    }

    for (final CellInfo ci : tm.getAllCellInfo()) {
      results.add(new ScanResult(loc, ci));
    }

    return results;
  }
}
