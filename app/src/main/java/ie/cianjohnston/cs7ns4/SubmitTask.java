package ie.cianjohnston.cs7ns4;

import android.os.AsyncTask;

import android.os.Handler;
import com.google.gson.Gson;
import ie.cianjohnston.cs7ns4.SubmitTask.SubmitTaskResult;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class SubmitTask extends AsyncTask<String, Void, SubmitTaskResult> {
  private final String TAG = "cs7ns4.SubmitTask";

  @Override
  protected SubmitTaskResult doInBackground(String... params) {
    HttpURLConnection conn = null;

    try {
      final URL url = new URL(params[0]);
      conn = (HttpURLConnection) url.openConnection();
      conn.setRequestProperty("X-Api-Key", params[1]);
      conn.setDoOutput(true);
      conn.setChunkedStreamingMode(0);
      final OutputStream os = new BufferedOutputStream(conn.getOutputStream());
      final BufferedWriter w =
          new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));

      w.write(params[2]);
      w.flush();
      w.close();
      os.close();
    } catch (final Exception e) {
      return new SubmitTaskResult(e);
    } finally {
      if (null != conn) {
        conn.disconnect();
      }
    }
    return new SubmitTaskResult(null);
  }

  class SubmitTaskResult {
    private Exception error;

    public Exception getError() {
      return error;
    }

    SubmitTaskResult(Exception e) {
      super();
      this.error = e;
    }
  }
}
