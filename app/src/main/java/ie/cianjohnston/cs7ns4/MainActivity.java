package ie.cianjohnston.cs7ns4;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.util.Log;

import androidx.preference.PreferenceManager;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
  private final String[] perms = {
    Manifest.permission.ACCESS_COARSE_LOCATION,
    Manifest.permission.ACCESS_FINE_LOCATION,
    Manifest.permission.READ_EXTERNAL_STORAGE,
    Manifest.permission.WRITE_EXTERNAL_STORAGE,
  };
  private final String TAG = "cs7ns4.MainActivity";
  private TelephonyManager telephonyManager;
  private LocationManager locationManager;

  private Scanner scanner;
  private ScanScheduler scanScheduler;

  private final LocationListener locationListener =
      new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
          Log.i(TAG, "location changed");
          Log.i(TAG, "lat: " + location.getLatitude());
          Log.i(TAG, "lng: " + location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
          Log.i(TAG, "location provider " + provider + " status changed");
        }

        @Override
        public void onProviderEnabled(String provider) {
          Log.i(TAG, "location provider " + provider + " enabled");
        }

        @Override
        public void onProviderDisabled(String provider) {
          Log.i(TAG, "location provider " + provider + " disabled");
        }
      };

  @SuppressLint("MissingPermission") // handled by ensurePerms()
  @Override
  protected void onStart() {
    super.onStart();
    this.locationManager.requestLocationUpdates(
        LocationManager.GPS_PROVIDER, 1000, 1, locationListener);

    // final ListView dump = findViewById(R.id.dump);
  }

  @Override
  protected void onStop() {
    super.onStop();
    locationManager.removeUpdates(locationListener);
  }

  @Override
  public void onRequestPermissionsResult(
      int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    if (requestCode != 0) {
      Log.e(TAG, "onRequestPermissionResult branch 1");
      return;
    }

    if (grantResults.length != 1) {
      Log.e(TAG, "onRequestPermissionResult branch 2");
      return;
    }

    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
      Log.e(TAG, "onRequestPermissionResult branch 3");
      return;
    }

    Log.e(TAG, "onRequestPermissionResult success");
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Toolbar t = findViewById(R.id.toolbar);
    setSupportActionBar(t);

    // Restart scanner upon preference change
    PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    final OnSharedPreferenceChangeListener listener =
        (sharedPreferences, key) -> {
          final String host = prefs.getString(SettingsActivity.KEY_PREF_HOST, "127.0.0.1");
          final int port =
              Integer.parseInt(prefs.getString(SettingsActivity.KEY_PREF_PORT, "8000"));
          final String apiKey = prefs.getString(SettingsActivity.KEY_PREF_API_KEY, "insecurekey");
          final int intervalMs =
              Integer.parseInt(prefs.getString(SettingsActivity.KEY_PREF_INTERVAL_MS, "10000"));
          final String submitUrl = String.format(Locale.ENGLISH, "http://%s:%d/cell", host, port);
          this.scanScheduler.setApiKey(apiKey);
          this.scanScheduler.setSubmitUrl(submitUrl);
          this.scanScheduler.setIntervalMs(intervalMs);
        };
    prefs.registerOnSharedPreferenceChangeListener(listener);

    ensurePerms();
    telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    initScanScheduler();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_settings) { // show settings UI
      final Intent intent = new Intent(this, SettingsActivity.class);
      startActivity(intent);
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  void initScanScheduler() {
    if (null != scanScheduler) {
      scanScheduler.stop();
    }
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    final String host = prefs.getString(SettingsActivity.KEY_PREF_HOST, "127.0.0.1");
    final int port = Integer.parseInt(prefs.getString(SettingsActivity.KEY_PREF_PORT, "8000"));
    final String apiKey = prefs.getString(SettingsActivity.KEY_PREF_API_KEY, "insecurekey");
    final int intervalMs =
        Integer.parseInt(prefs.getString(SettingsActivity.KEY_PREF_INTERVAL_MS, "10000"));

    scanner = new Scanner(telephonyManager, locationManager);
    final String submitUrl = String.format(Locale.ENGLISH, "http://%s:%d/cell", host, port);
    scanScheduler = new ScanScheduler(scanner, submitUrl, apiKey, intervalMs);

    final Button b = findViewById(R.id.start_stop_button);
    b.setOnClickListener((v) -> scanScheduler.toggle());
  }

  void ensurePerms() {
    boolean needToAsk = false;
    for (final String perm : perms) {
      if (ActivityCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
        needToAsk = true;
        break;
      }
    }
    if (needToAsk) {
      ActivityCompat.requestPermissions(this, perms, 0);
    }
  }
}
