package ie.cianjohnston.cs7ns4;

import android.location.Location;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;

class ScanResult {
  final long ts;
  final Double lat;
  final Double lng;
  final Double alt;
  final Float acc;

  final String type;
  final int cid;
  final int taclac;
  final int mcc;
  final int mnc;
  final int dbm;
  final int asu;
  final int lvl;

  ScanResult(final Location loc, final CellInfo cellInfo) {
    this.ts = System.currentTimeMillis();
    this.lat = loc.getLatitude();
    this.lng = loc.getLongitude();
    this.alt = loc.getAltitude();
    this.acc = loc.getAccuracy();

    if (cellInfo instanceof CellInfoGsm) {
      final CellInfoGsm ci = (CellInfoGsm) cellInfo;
      this.type = "gsm";
      this.cid = ci.getCellIdentity().getCid();
      this.taclac = ci.getCellIdentity().getLac();
      this.mcc = forceInt(ci.getCellIdentity().getMccString());
      this.mnc = forceInt(ci.getCellIdentity().getMncString());
      this.dbm = ci.getCellSignalStrength().getDbm();
      this.asu = ci.getCellSignalStrength().getAsuLevel();
      this.lvl = ci.getCellSignalStrength().getLevel();
    } else if (cellInfo instanceof CellInfoWcdma) {
      final CellInfoWcdma ci = (CellInfoWcdma) cellInfo;
      this.type = "wcdma";
      this.cid = ci.getCellIdentity().getCid();
      this.taclac = ci.getCellIdentity().getLac();
      this.mcc = forceInt(ci.getCellIdentity().getMccString());
      this.mnc = forceInt(ci.getCellIdentity().getMncString());
      this.dbm = ci.getCellSignalStrength().getDbm();
      this.asu = ci.getCellSignalStrength().getAsuLevel();
      this.lvl = ci.getCellSignalStrength().getLevel();
    } else if (cellInfo instanceof CellInfoLte) {
      final CellInfoLte ci = (CellInfoLte) cellInfo;
      this.type = "wcdma";
      this.cid = ci.getCellIdentity().getCi();
      this.taclac = ci.getCellIdentity().getTac();
      this.mcc = forceInt(ci.getCellIdentity().getMccString());
      this.mnc = forceInt(ci.getCellIdentity().getMncString());
      this.dbm = ci.getCellSignalStrength().getDbm();
      this.asu = ci.getCellSignalStrength().getAsuLevel();
      this.lvl = ci.getCellSignalStrength().getLevel();
    } else {
      throw new IllegalArgumentException(
          "unhandled CellInfo type: " + cellInfo.getClass().getName());
    }
  }

  private int forceInt(final String s) {
    try {
      return Integer.parseInt(s);
    } catch (final NumberFormatException e) {
      return 0;
    }
  }
}
