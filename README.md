# CS7NS4 Network Tower Scanning Application

This is an Android application that polls the signal strength and other information from surrounding cell towers and persists the data to disk.

## Building:

Run `./gradlew build`

## Installing:

 * Ensure your device has USB debugging enabled.
 * Connect your device via USB and accept the prompt.
 * Run `./gradlew installDebug`
 * You should now be able to run the `CS7NS4` application. It will start collecting data automatically, once opened.

## See Also:

* [MozStumbler](https://github.com/mozilla/MozStumbler/) is a similar application that submits the data to an open online service, however, it sadly appears to be broken on newer versions of Android.
